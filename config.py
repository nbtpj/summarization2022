import os

from thinc.api import Config

CONF = Config().from_disk(f"{os.path.dirname(__file__)}/config.cfg")


def reload_config():
    global CONF
    CONF = Config().from_disk(f"{os.path.dirname(__file__)}/config.cfg")
    return CONF
