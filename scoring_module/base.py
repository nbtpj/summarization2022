from __future__ import annotations
from typing import Iterable, Optional, Any
from spacy.tokens import Doc


class ScoringModule:
    """
    Module tính điểm trọng số cho từng câu trong văn bản.
    Các lớp thừa kế class này làm nhiệm vụ tính điểm các câu và lưu vào biến .use_data['score_list'] của từng doc.
    Lớp này KHÔNG SINH RA TÓM TẮT CUỐI.
    """

    def __init__(self, name, *args, **kwargs):
        self.name = name

    def assign_score_to_doc(self, scores: Iterable[float], doc: Doc) -> bool:
        if 'score_list' not in doc.user_data:
            doc.user_data['score_list'] = [dict() for _ in doc.sents]
        scores = list(scores)
        n_sent = len(doc.user_data['score_list'])
        if len(scores) != n_sent:
            raise Exception(f"Văn bản có {n_sent} câu, trong khi có {len(scores)} điểm gán!")
        for idx, score in zip(range(n_sent), scores):
            doc.user_data['score_list'][idx][self.name] = score
        return True

    def train(self, Xs: Iterable[Iterable[Doc]], ys: Optional[Iterable[Any]], **kwargs) \
            -> Optional[Iterable[Any]]:
        """
        hàm sử dụng để huấn luyện mô hình(đối với các mô hình cần huấn luyện trước)
        :param Xs: Tập đa văn bản huấn luyện, cấu trúc gồ một danh sách các đă văn bản, trong đó đa văn bản được thể hiện bằng một tập các văn bản spacy.token.Doc
        :param ys: Tập nhãn (Optional), tùy vào mô hình có thể dùng hay không dùng, và kiểu dữ liệu cũng không cố định
        :param kwargs: Các tham số khác
        :return: Có thể tùy ý trả về, không ràng buộc.
        """
        raise NotImplementedError

    def predict(self, Xs: Iterable[Iterable[Doc]], **kwargs) -> Iterable[Iterable[Iterable[float]]]:
        """
        hàm sử dụng để đưa ra điểm cho tập đa văn bản cụ thể (có thể có hoặc không dựa trên các mô hình huấn luyện trước).
        Các điểm này được tính chi tiết đến mức câu (nhiều cụm đa vb > đa vb > đơn vb > câu)
        :param Xs: Tập đa văn bản cần được tính điểm
        :param kwargs: Các tham số khác
        :return: Tập các đối tượng thể hiện điểm (tùy
        """
        raise NotImplementedError
