from __future__ import annotations

from typing import Iterable, List
from ..base import ScoringModule
from config import CONF
from spacy.tokens import Doc, Span
from lexrank import LexRank
from lexrank.algorithms.power_method import stationary_distribution
from util.spacy_config import nlp
from util.concurrent import executor
import numpy as np


class EmbeddingBasedLexrank(LexRank):
    """
    Đây là bản custom lại lexrank sử dụng độ tương đồng các câu dựa trên vector bert embedding của các câu đó.
    Việc "huấn luyện" (khởi tạo các documents) như cách sử dụng cũ sẽ KHÔNG DIỄN RA, bởi không sử dụng giá trị idf trong so sánh tương đồng câu.
    """

    def __init__(self, **kwargs):
        super(EmbeddingBasedLexrank, self).__init__(documents=None, **kwargs)

    def _calculate_idf(self, *args, **kwargs):
        pass

    def _calculate_similarity_matrix(self, sentence_vector: np.array):
        # chuẩn hóa các vector câu về L2 để khi nhân được giá trị cos
        to_mul = np.linalg.norm(sentence_vector, axis=1).reshape((-1, 1))
        to_mul = sentence_vector / to_mul
        return np.matmul(to_mul, to_mul.T)

    def rank_sentences(
            self,
            sentences: np.array,
            threshold=None,
            fast_power_method=True,
            **kwargs
    ):

        similarity_matrix = self._calculate_similarity_matrix(sentences)

        if threshold is None:
            markov_matrix = self._markov_matrix(similarity_matrix)

        else:
            markov_matrix = self._markov_matrix_discrete(
                similarity_matrix,
                threshold=threshold,
            )
        scores = stationary_distribution(
            markov_matrix,
            increase_power=fast_power_method,
            normalized=False,
        )
        return scores


def preprocess(docs: Iterable[Doc | Span]) -> List[str]:
    """
    Tiền xử lý các cụm văn bản trước khi đưa vào tính điểm lexrank cũng như huấn luyện.
    Tuy hàm này chỉ sử dụng khi config LexRankModule có ['type'] = 'tfidf' (mặc định là 'embedding')
    :param docs: cụm văn bản
    :return: cụm sau xử lý ở dạng str
    """
    result = []
    for doc in docs:
        string_doc = []
        for token in doc:
            if not any([token.is_punct, token.is_space, token.like_num, token.is_stop]):
                string_doc.append(token.norm_)
        result.append(' '.join(string_doc))
    return result


class LexRankModule(ScoringModule):
    def __init__(self, conf=None):
        """
        Khởi tạo đối tượng tính toán
        :param conf: nếu conf['type']=='tfidf' -> sử dụng mô hình lexrank của thư viện, nếu giá trị khác thì sẽ dùng bản custom tính toán trên vector bert embedding
        """

        if not conf:
            self.conf = CONF['lexrank']
        else:
            self.conf = conf
        super(LexRankModule, self).__init__(**self.conf)
        if self.conf['type'] == 'tfidf':
            self.model = None
        else:
            self.model = EmbeddingBasedLexrank()

    def train(self, *args, **kwargs):
        """
        đây là mô hình tính điểm nên việc huấn luyện trước là không cần thiết.
        Xme chi tiết tại hàm predict
        :param kwargs:
        :return:
        """
        return None

    def score_each_cluster(self, X: Iterable[Doc]) -> Iterable[float]:
        """
        Nếu self.config['type'] = tfidf: mô hình truyền thống sử dụng vector tfidf với idf được tính toàn cụ (toàn bộ tập Xs) để so sánh câu.
        Ngược lại, so sánh câu sẽ dựa trên vector bert embedding, do không sử dụng đến thành phần idf, nên các điểm lexrank nên được tính toán theo cụm độc lập.
        (Với sentence sim tính dựa vào giá trị .use_data['bert_sentence_vector'] (vector embedding bert))
        :param X: Một cụm văn bản chứa các câu cần score
        :return: Một list các điểm tương ứng với vị trí các câu. Thực tế điểm này cũng được lưu trong doc.user_data['score_list']['lexrank'].
        """
        if self.conf['type'] != 'tfidf':
            sentence_vector_per_doc = [doc.user_data['bert_sentence_vector'] for doc in X]
            merge_sentences = np.concatenate(sentence_vector_per_doc, axis=0)
        else:
            merge_sentences = preprocess([sent for doc in X for sent in doc.sents])
        scores = self.model.rank_sentences(merge_sentences)
        i = 0
        for doc in X:
            n_sent = len(list(doc.sents))
            self.assign_score_to_doc(scores[i:i + n_sent], doc)
            i += n_sent
        return scores

    def predict(self, Xs: Iterable[Iterable[Doc]], **kwargs) -> Iterable[Iterable[float]]:
        """
        Tính điểm lexrank từng câu dựa trên đầu vào.
        Nếu self.conf['type'] == 'tfidf', mô hình sẽ tính giá trị idf dựa trên toàn bộ các văn bản trong Xs.
        :param Xs:
        :param kwargs:
        :return:
        """
        if self.conf['type'] == 'tfidf':
            docs = [doc for X in Xs for doc in X]
            self.model = LexRank(preprocess(docs), stopwords=nlp.Defaults.stop_words)
        jobs = [executor.submit(self.score_each_cluster, X=X) for X in Xs]
        rs = [job.result() for job in jobs]
        return rs
