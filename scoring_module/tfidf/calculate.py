from typing import Iterable

from ..base import ScoringModule
from config import CONF
from spacy.tokens import Doc, Span
from util.concurrent import executor
from util.data_loader import VOCAB
from sklearn.feature_extraction.text import TfidfVectorizer


def tokenizer(doc: Span):
    """
    Sử dụng các token đã tách sẵn từ spacy.token.Doc để đưa vào mô hình mà khoogn sử dụng hàm token của mô hình tfidf.
    Các token này:
        - không phải từ dừng
        - không phải các kí tự "space"
        - không giống số
        - không phải các kí tự punctuation
    :param doc: document
    :return: list of tokens and entities
    """
    if doc:
        rs = []
        for token in doc:
            if not any([token.is_punct, token.is_space, token.like_num, token.is_stop]):
                rs.append(token.norm_)
        return rs
    else:
        return []


class TfIdfModule(ScoringModule):
    def __init__(self, conf=None, vocab=None):
        """
        Khởi tạo đối tượng tính toán tfidf.
        :param conf: chưa định nghĩa
        """
        if not conf:
            self.conf = CONF['tfidf']
        else:
            self.conf = conf
        if not vocab:
            vocab = VOCAB
        self.vectorizer = TfidfVectorizer(tokenizer=tokenizer, lowercase=False, token_pattern=None, vocabulary=vocab)

    def local_train(self, Xs: Iterable[Doc]):
        """
        tạo và gán các vector tidf (lưu ý đây là các vector thưa) vào từng văn bản trong một tập đa văn bản.
        sau bước này, toàn bộ các câu sẽ có một vector tfidf lưu trong phần user_data
            (xem file calculate_tfidf_for_each_sentence để xem cách sử dụng cho từng câu)
        :param Xs: tập đa văn bản
        :return: 1 nếu thành công
        """
        sents = []
        for doc in Xs:
            for sent in doc.sents:
                sents.append(sent)
        self.vectorizer.fit(sents)
        for doc in Xs:
            try:
                vector_tfidf = self.vectorizer.transform(doc.sents)
                doc.user_data['tfidf_vector'] = vector_tfidf
            except Exception as e:
                print(e, doc)
                print(doc)

        return 1

    def train(self, Xs: Iterable[Iterable[Doc]], **kwargs):
        """
        tạo các vector tfidf cho nhiều cụm đa văn bản.
        :param Xs: tập các cụm đa văn bản, mỗi cụm có nhiều đơn văn bản (đối tượng spacy.token.Doc)
        :param kwargs: các tham số khác
        :return: các trạng thái bao rằng các luồng xử lý đã xong
        """
        list_work = [executor.submit(self.local_train, docs) for docs in Xs]
        return [work.result(timeout=None) for work in list_work]

    def calculate_tfidf_for_each_sentence(self, docs: Iterable[Doc]) -> Iterable[Iterable[float]]:
        """
        tính toán điểm tfidf cho từng câu.
        Đầu ra là MỘT SỐ THỰC CHO MỖI CÂU.
        Điểm này sau đó được gán cho từng câu trong phần user_data.
        :param docs:  danh sách các doc cần tính toán
        :return: danh sách các điểm tương ứng với các câu, trong cụm đa văn bản.
        """
        rs = []
        for doc in docs:
            if "tfidf_vector" not in doc.user_data:
                raise Exception(f"Điểm tfidf chưa được tính toán cho văn bản {doc.text}")
            score = []
            if 'score_list' not in doc.user_data:
                doc.user_data['score_list'] = [dict() for _ in doc.sents]

            for sent, vector, idx in zip(doc.sents,
                                         doc.user_data['tfidf_vector'],
                                         range(len(doc.user_data['score_list']))):
                sen_score = vector.sum()
                score.append(sen_score)
                doc.user_data['score_list'][idx]['tfidf'] = sen_score

            rs.append(score)
        return rs

    def predict(self, Xs: Iterable[Iterable[Doc]], train=True, **kwargs) -> Iterable[Iterable[Iterable[float]]]:
        """
        tính điểm cho từng câu trong tập các cụm đa văn bản.
        Điểm tfidf này được tính DỰA TRÊN TỪNG CỤM ĐA VĂN BẢN, có nghĩa là điểm idf được tính trên toàn bộ các câu trong nhiều đơn văn bản thuộc cùng 1 cụm đa vb.
        :param Xs: tập các cụm đa văn bản, mỗi cụm có nhiều đơn văn bản (đối tượng spacy.token.Doc)
        :param train: Có cần tính lại vector tfidf cho từng câu trong các cụm đa vb hay không?
        :param kwargs: Các tham số khác
        :return: danh sách điểm tfidf
        """
        if train:
            self.train(Xs)
        list_work = [executor.submit(self.calculate_tfidf_for_each_sentence, docs) for docs in Xs]
        return [work.result(timeout=None) for work in list_work]
