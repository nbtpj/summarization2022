# summarization2022



## Dữ liệu


Có 4 tập dữ liệu đã qua tiền xử lý, tương ứng với 4 file trong thư mục [data/](https://drive.google.com/drive/folders/104upsjGx0HI9SUfEAq1asQuy7JvjCiC_?usp=sharing):

* **ViMs-abs.zip**: Dữ liệu của tập ViMs, với phần summs (Ys) là các tóm tắt abstract (gold) trong folder ViMs/summary
* **ViMs-ext.zip**: Dữ liệu của tập ViMs, với phần summs (Ys) là các tóm tắt extract (nối các câu có nhãn 1 với nhau) trong folder ViMs/S3_summary
* **VN-MDS-abs.zip**: Dữ liệu của tập VN-MDS, với phần docs (Xs) và summs (Ys) chưa qua xử lý tokenize, và tóm tắt là của ref_1, ref_2
* **VN-MDS-abs-tokenized.zip**: Dữ liệu của tập VN-MDS, với phần docs (Xs) và summs (Ys) đã qua xử lý tokenize, và tóm tắt là của ref_1, ref_2
---
**Tiền xử lý**
Sau bước tiền xử lý, các văn bản cũng như các tóm tắt được trả về dạng spacy doc, trong đó **ĐÃ QUA XỬ LÝ TOKENIZE, NER, POS-TAG , CHIA CÂU, BERT EMBEDDING**.
Để có thể sử dụng các bộ dữ liệu này, giải nén các file trên trong thư mục data, sau đó trong file **config.cfg**, đặt các giá trị:

[data_loader]

**data_name** = tên folder vừa giải nén ra

**is_new_data** = true

Hãy kiểm tra lại rằng trong folder vữa giải nén có 1 file **origin.json**. 
File này được sử dụng để điều khiển quá trình đọc dữ liệu.

Hoặc chạy dòng lệnh:

```
>> from util.data_loader import get_examples
>> Xs, Ys = [], []
>> for docs, summs in tqdm.tqdm(get_examples()):
...    Xs.append(docs)
...    Ys.append(summs)
...    if len(Xs)==10:
...        break
```

---


###1. VN-MDS

**Dữ liệu gốc**: data/KTLAB-200document-clusters.zip

**Dữ liệu sau tổng hợp**: data/VN-MDS.json


###2. ViMs

**Dữ liệu gốc**: data/ViMs.zip

**Dữ liệu sau tổng hợp**: data/ViMs.json

## Demo

xem xét demo sử dụng tại [đây](notebook/demo.ipynb)


[comment]: <> (## Add your files)

[comment]: <> (- [ ] [Create]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file&#41; or [upload]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file&#41; files)

[comment]: <> (- [ ] [Add files using the command line]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line&#41; or push an existing Git repository with the following command:)

[comment]: <> (```)

[comment]: <> (cd existing_repo)

[comment]: <> (git remote add origin https://gitlab.com/nbtpj/summarization2022.git)

[comment]: <> (git branch -M main)

[comment]: <> (git push -uf origin main)

[comment]: <> (```)

[comment]: <> (## Integrate with your tools)

[comment]: <> (- [ ] [Set up project integrations]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://gitlab.com/nbtpj/summarization2022/-/settings/integrations&#41;)

[comment]: <> (## Collaborate with your team)

[comment]: <> (- [ ] [Invite team members and collaborators]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/members/&#41;)

[comment]: <> (- [ ] [Create a new merge request]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html&#41;)

[comment]: <> (- [ ] [Automatically close issues from merge requests]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically&#41;)

[comment]: <> (- [ ] [Enable merge request approvals]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/&#41;)

[comment]: <> (- [ ] [Automatically merge when pipeline succeeds]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html&#41;)

[comment]: <> (## Test and Deploy)

[comment]: <> (Use the built-in continuous integration in GitLab.)

[comment]: <> (- [ ] [Get started with GitLab CI/CD]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/ci/quick_start/index.html&#41;)

[comment]: <> (- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing&#40;SAST&#41;]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/application_security/sast/&#41;)

[comment]: <> (- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/topics/autodevops/requirements.html&#41;)

[comment]: <> (- [ ] [Use pull-based deployments for improved Kubernetes management]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/user/clusters/agent/&#41;)

[comment]: <> (- [ ] [Set up protected environments]&#40;https://gitlab.com/-/experiment/new_project_readme_content:2aaf55a8258abcc719ed3441ca2456de?https://docs.gitlab.com/ee/ci/environments/protected_environments.html&#41;)

[comment]: <> (***)

[comment]: <> (# Editing this README)

[comment]: <> (When you're ready to make this README your own, just edit this file and use the handy template below &#40;or feel free to structure it however you want - this is just a starting point!&#41;.  Thank you to [makeareadme.com]&#40;https://www.makeareadme.com&#41; for this template.)

[comment]: <> (## Suggestions for a good README)

[comment]: <> (Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.)

[comment]: <> (## Name)

[comment]: <> (Choose a self-explaining name for your project.)

[comment]: <> (## Description)

[comment]: <> (Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.)

[comment]: <> (## Badges)

[comment]: <> (On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.)

[comment]: <> (## Visuals)

[comment]: <> (Depending on what you are making, it can be a good idea to include screenshots or even a video &#40;you'll frequently see GIFs rather than actual videos&#41;. Tools like ttygif can help, but check out Asciinema for a more sophisticated method.)

[comment]: <> (## Installation)

[comment]: <> (Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.)

[comment]: <> (## Usage)

[comment]: <> (Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.)

[comment]: <> (## Support)

[comment]: <> (Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.)

[comment]: <> (## Roadmap)

[comment]: <> (If you have ideas for releases in the future, it is a good idea to list them in the README.)

[comment]: <> (## Contributing)

[comment]: <> (State if you are open to contributions and what your requirements are for accepting them.)

[comment]: <> (For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.)

[comment]: <> (You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.)

[comment]: <> (## Authors and acknowledgment)

[comment]: <> (Show your appreciation to those who have contributed to the project.)

[comment]: <> (## License)

[comment]: <> (For open source projects, say how it is licensed.)

[comment]: <> (## Project status)

[comment]: <> (If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.)

