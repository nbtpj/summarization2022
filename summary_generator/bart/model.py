from __future__ import annotations
from typing import Iterable, List
from spacy.tokens import Doc
from transformers import MBartForConditionalGeneration, AutoTokenizer
from .util import seq2seq, any2docs
from summary_generator.base import SummarizingModule
import os
from os.path import join
import tqdm

MODEL_DIR = f"{os.path.dirname(__file__)}/../../model"


class BartGenerator(SummarizingModule):
    def __init__(self, path=None, device='cpu', **kwargs):
        if not path:
            path = join(MODEL_DIR, 'BARTpho_tuneon_vnds')
        self.device = device
        self.path = path
        self.tokenizer = AutoTokenizer.from_pretrained(self.path, use_fast=False)
        self.model = MBartForConditionalGeneration.from_pretrained(self.path).to(device)

    def generate(self, Xs: Iterable[Iterable[Doc | str] | Doc | str],
                 desegment: bool = False, skip_special_tokens: bool = False,
                 batch_size=1, **kwargs) -> List[str]:
        out_texts = []
        data = any2docs(Xs)
        for i in tqdm.tqdm(range(0, len(data), batch_size), desc='generating process'):
            batch = data[i:i + batch_size]
            txts = seq2seq(tokenizer=self.tokenizer, model=self.model, batch=batch,
                           is_split_into_words=False, desegment=desegment,
                           skip_special_tokens=skip_special_tokens, device=self.device)
            out_texts.extend(txts)
        return out_texts

    def __call__(self, *args, **kwargs):
        return self.generate(*args, **kwargs)

    def summary(self, Xs: Iterable[Iterable[Doc | str] | Doc | str], **kwargs) -> Iterable[str | Doc]:
        return self.generate(Xs)

    def train(self, **kwargs):
        pass
