from __future__ import annotations

from typing import Iterable, List
import torch
from spacy.tokens import Doc

softmax = torch.nn.Softmax(-1)


def any2docs(Xs: Iterable[Iterable[Doc | str] | Doc | str]) -> List[List[str]]:
    tokens = []
    for X in Xs:
        if isinstance(X, Doc) or isinstance(X, str):
            # each sample is single doc
            cluster_or_doc_doc = str(X)
        else:
            # each sample is multi doc
            cluster_or_doc_doc = []
            for doc in X:
                cluster_or_doc_doc.append(str(doc))
            cluster_or_doc_doc = ' '.join(cluster_or_doc_doc)
        tokens.append(cluster_or_doc_doc)
    return tokens


def state2ids(logits, toids, device) -> torch.Tensor:
    sf = softmax(logits)
    if toids:
        output_ids = torch.argmax(sf, dim=-1).to(device)
        return output_ids
    else:
        return sf


def ids2text(tokenizer, ids, desegment, skip_special_tokens) -> List[str]:
    deseg = lambda tokens: [token.replace('_', ' ') if desegment else token for token in tokens]
    tok_articles = [
        deseg(tokenizer.convert_ids_to_tokens(sub, skip_special_tokens=skip_special_tokens))
        for sub in ids]
    texts = [tokenizer.convert_tokens_to_string(art) for art in tok_articles]
    return texts


def seq2seq(tokenizer, model, batch, is_split_into_words, desegment, skip_special_tokens, device):
    inputs = tokenizer(batch, padding=True, truncation=True, is_split_into_words=is_split_into_words,
                       max_length=model.config.max_position_embeddings,
                       return_tensors="pt").to(device)
    if 'token_type_ids':
        del inputs['token_type_ids']
    outputs = model(**inputs)
    output_ids = state2ids(outputs.logits, True, device)
    return ids2text(tokenizer, output_ids, desegment, skip_special_tokens)
