from __future__ import annotations

from typing import Iterable
from spacy.tokens import Doc


class SummarizingModule:
    """
    interface chung sử dụng cho quá trình tạo tóm tắt.
    Các lớp thừa kế lớp này SINH RA TÓM TẮT CUỐI.
    """
    def train(self, Xs: Iterable[Iterable[Doc | str] | Doc | str], Ys: Iterable[Iterable[Doc | str] | Doc | str],
              **kwargs):
        """
        Huấn luyện các mô hình cần huấn luyện.
        :param Xs:
        :param Ys:
        :param kwargs:
        :return:
        """
        raise NotImplementedError

    def summary(self, Xs: Iterable[Iterable[Doc | str] | Doc | str], **kwargs) -> Iterable[str | Doc]:
        """
        Hàm tạo ra tóm tắt cuối từ input đầu vào.
        :param Xs:
        :param kwargs:
        :return:
        """
        raise NotImplementedError
