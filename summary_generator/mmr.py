from __future__ import annotations

import copy
import logging
from util.concurrent import executor
from summary_generator.base import *
from typing import Callable, Union, Any
from util.spacy_config import create_doc
import numpy as np


def get_score(query, doc, result, sim, _lambda, _thresh):
    if len(result) == 0:
        score = _lambda * sim(query, doc)
    else:
        score = _lambda * sim(query, doc) - (1 - _lambda) * np.amax(
            [sim(doc, d) for d in result], axis=0)
    if isinstance(_thresh, float) and score < _thresh:
        return -1e4
    return score


def get_top_n(query, collection_docs, sim, _lambda, n_keep, n_remove, _thresh):
    """
    Lấy ra top n_keep câu sử dụng đánh giá mmr. kết quả cuối đã được sort lại theo vị trí ban đầu
    :param query: đối tượng đại diện cho câu truy vấn. một đối tượng có thể feed vào hàm sim
    :param collection_docs: tập các đối tượng đại diện cho câu, với mỗi câu là một đối tượng có thể feed vào hàm sim
    :param sim: hàm nhận 2 đầu vào và đầu ra là 1 số float biểu diễn tính giống nhau
    :param _lambda: tham số lambda trong công thức mmr
    :param n_keep: lượng câu cần giữ
    :param n_remove: lượng câu cần bỏ. Lưu ý là chỉ 1 trong 2 tham số n_keep và n_remove != None
    :param _thresh: ngưỡng cắt. Nếu ngưỡng này khác None, mọi score dưới ngưỡng này đều bị đẩy về -1e4
    :return: danh sách các CHỈ SỐ của đối tượng trong collection_docs đã qua mmr và sort lại theo thứ tự feed vào ban đầu
    """
    assert not (n_keep is not None and n_remove is not None), "only n_keep or n_remove should be fed"
    collection_docs = copy.copy(collection_docs)
    if n_keep and n_keep > len(collection_docs):
        n_keep = len(collection_docs)
    if n_remove and n_keep < len(collection_docs):
        n_keep = len(collection_docs) - n_remove
    result = []
    r = []
    pos = list(range(len(collection_docs)))

    for _ in range(n_keep):
        if len(collection_docs) == 0:
            break
        id = np.argmax(
            [get_score(query=query, doc=doc, result=result, _lambda=_lambda, sim=sim, _thresh=_thresh) for doc in
             collection_docs])
        d = (collection_docs[id], pos[id])
        result.append(collection_docs[id])
        r.append(d)
        del collection_docs[id]
        del pos[id]
    r.sort(key=lambda x: x[1])
    result = [pair[1] for pair in r]
    return result


class MmrSummaizing(SummarizingModule):
    def __init__(self):
        super(MmrSummaizing, self).__init__()

    def train(self, Xs: Iterable[Iterable[Doc | str] | Doc | str], Ys: Iterable[Iterable[Doc | str] | Doc | str],
              **kwargs):
        logging.INFO("MMR is NOT need training!")
        return self

    def _mmr_each_sample(self, X: Iterable[Doc | str], Q: Union[Doc | str] = None,
                         _lambda: float = 0.0, sim: Callable = None,
                         n_keep: int = 10, thresh: float = None,
                         n_remove: int = None) -> str:
        sents = []
        scores = []
        for each_doc in X:
            if isinstance(X, str):
                each_doc = create_doc(each_doc)
            sents.extend([sent for sent in each_doc.sents])
            if 'tfidf_vector' in each_doc.user_data:
                scores.extend(sen_vec for sen_vec in each_doc.user_data['tfidf_vector'])
        """
        định nghĩa hàm similarity. vÍ dụ sử dụng cos giữa 2 vector 
        """

        def cos(a, b):
            """
            định nghĩa này, bởi Q feed vào là None, nên sẽ chỉ tính được sim giữa các câu trong collenction doc.
            """
            if a is None or b is None:
                return 0
            c = a*b.T
            return c.toarray()[0][0]

        if sim is None:
            sim = cos
        """
        ví dụ cài đặt này sử dụng vector tfidf chẳng hạn
        """
        sents_vects = scores
        idx = get_top_n(
            query=Q,
            collection_docs=sents_vects,
            _lambda=_lambda,
            n_keep=n_keep,
            sim=sim,
            _thresh=thresh,
            n_remove=n_remove)
        return ' '.join([str(sents[i]) for i in idx])

    def summary(self, Xs: Iterable[Iterable[Doc | str]],
                Qs: Iterable[Any] = None, _lambda: float = 0.0, sim: Callable = None,
                thresh: float = None, n_keep: int = 10,
                n_remove: int = None,
                **kwargs) -> Iterable[str]:
        """
        :param Xs: Tập đa văn bản
        :param Qs: Tập đối tượng đại diện cho query tương ứng. cần phải nhớ rằng, các đối tượng trong Qs sẽ được FEED TRỰC TIẾP
        vào hàm sim, nên phải đảm bảo hàm sim có thể handle được điều này, hoặc các đối tượng Q phải đúng kiểu.
        Ví dụ: nếu tính = tfidf chẳng hạn, thì mỗi Q tỏng Qs sẽ là 1 vector tfidf, chứ ko phải 1 doc hay 1 str nếu hàm sim định
        nghĩa sẽ tính cos (như trong bản cài mẫu này)
        :param sim: hàm nhận 2 đầu vào và đầu ra là 1 số float biểu diễn tính giống nhau
        :param _lambda: tham số lambda trong công thức mmr
        :param n_keep: lượng câu cần giữ
        :param n_remove: lượng câu cần bỏ. Lưu ý là chỉ 1 trong 2 tham số n_keep và n_remove != None
        :param thresh: ngưỡng cắt. Nếu ngưỡng này khác None, mọi score dưới ngưỡng này đều bị đẩy về -1e4
        :param kwargs:
        :return:
        """
        if Qs is not None:
            list_work = [executor.submit(self._mmr_each_sample, X=X, thresh=thresh, n_remove=n_remove,
                                         n_keep=n_keep, Q=Q, _lambda=_lambda) for X, Q in zip(Xs, Qs)]
        else:
            list_work = [executor.submit(self._mmr_each_sample, X=X, thresh=thresh, n_remove=n_remove,
                                         n_keep=n_keep, Q=None, _lambda=_lambda) for X in Xs]
        return [work.result(timeout=None) for work in list_work]


if __name__ == '__main__':
    """ unit test"""
    import tqdm
    from util.data_loader import get_examples

    Xs, Ys = [], []
    for docs, summs in tqdm.tqdm(get_examples()):
        Xs.append(docs)
        Ys.append(summs)
        if len(Xs) == 3:
            break
    from scoring_module import TextRankModule, LexRankModule, TfIdfModule

    pipeline = [
        TextRankModule(),
        TfIdfModule(),
        LexRankModule()
    ]
    for pipe in pipeline:
        pipe.train(Xs)
        pipe.predict(Xs)

    import pandas as pd

    score_df = None
    for X in tqdm.tqdm(Xs):
        for doc in X:
            if score_df is None:
                score_df = pd.DataFrame().from_records(doc.user_data['score_list'])
            else:
                score_df.append(pd.DataFrame().from_records(doc.user_data['score_list']))
    print(score_df.head(5))
    from summary_generator import MmrSummaizing

    gen = MmrSummaizing()
    rs = gen.summary(Xs[:2], n_keep=5)
    print(rs)
    print(Xs[:2])
