from concurrent.futures.thread import ThreadPoolExecutor

import requests
import threading
from config import CONF

thread_local = threading.local()


def get_session():
    if not hasattr(thread_local, "session"):
        thread_local.session = requests.Session()
    return thread_local.session


max_thread = CONF['system']['max_cpu_thread']
executor = ThreadPoolExecutor(max_workers=max_thread)
