from abbreviations import schwartz_hearst
from config import CONF
import re


def clean_abbreviation(text: str) -> str:
    """
    Đưa các dạng viết tắt về dạng đầy đủ
    """
    pairs = schwartz_hearst.extract_abbreviation_definition_pairs(doc_text=text)
    for short, full in pairs.items():
        text = text.replace(short, full)
    return text


def clean_non_utf8(text: str) -> str:
    """
    Loại bỏ các ký tự không phải utf-8
    """
    text = text.encode("utf-8", errors="ignore").decode()
    return text


TAG_RE = re.compile(r'<[^>]+>')


def clean_html(text: str) -> str:
    """
    Thay thế các thẻ <> (html) thành kí tự space
    """
    return TAG_RE.sub(' ', text)


def space_normalize(text: str) -> str:
    """
    Chuẩn hóa các ký tự trống : space, '\t', '\n', vv..
    """
    return ' '.join(text.split())


"""
 Các hàm trong phần cài đặt dưới có tham khảo tại `"Phân loại văn bản tiếng Việt sử dụng machine learning"
 <https://nguyenvanhieu.vn/phan-loai-van-ban-tieng-viet/>' của tác giả Nguyễn Văn Hiếu, với mục tiêu làm "mịn" hơn các
 văn bản tiếng việt để cải thiện các mô hình xử lý ngôn ngữ.
"""

uniChars = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ"
unsignChars = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIOOOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU"
charutf8 = "à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ".split(
    '|')
char1252 = 'à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ'.split(
    '|')
dicchar = {}
for i in range(len(char1252)):
    dicchar[char1252[i]] = charutf8[i]

vowel = ['a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y']
vowel.extend([c.upper() for c in vowel])
consonant = ['b', 'c', 'd', 'đ', 'g', 'h', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x']
consonant.extend([c.upper() for c in consonant])
sign = ['<grave accent>', '<acute>', '<ques>', '<tilde>', '<dot>']
vowel_with_sign = [[v, s] for v in vowel for s in sign]
vowel_with_sign = {c: c_w_s for c, c_w_s in zip(charutf8, vowel_with_sign)}
sign_with_vowel = {''.join(v): k for k, v in vowel_with_sign.items()}


def normalize_sign(word):
    vs = []
    s = None
    for c in word:
        if c in vowel_with_sign:
            v, s = vowel_with_sign[c]
            vs.append(v)
        else:
            vs.append(c)
    if s is None:
        return word
    l_w = ''.join(vs).lower()
    vs_list = [x for x in l_w if x in vowel]
    if len(vs_list) == 1:
        where_to_put = l_w.find(vs_list[0])
    elif 'ê' in l_w:
        where_to_put = l_w.find('ê')
    elif 'ơ' in l_w:
        where_to_put = l_w.find('ơ')
    elif 'ô' in l_w:
        where_to_put = l_w.find('ô')
    elif 'â' in l_w:
        where_to_put = l_w.find('â')
    elif 'ă' in l_w:
        where_to_put = l_w.find('ă')
    elif 'a' in vs_list and 'a' != vs_list[-1]:
        where_to_put = l_w.find('a')
    else:
        where_to_put = l_w.find(vs_list[-2]) if len(vs_list) == 3 else l_w.find(vs_list[1])
        for d_v in ['ia', 'ya', 'ua', 'ưa']:
            if d_v in l_w:
                where_to_put = l_w.find(d_v)
        if (l_w[0] == 'q' and 'ua' in l_w) or (l_w[0] == 'g' and 'ia' in l_w):
            where_to_put = l_w.find('a')
        if (l_w[0] != 'q' and 'ua' in l_w) or (l_w[0] != 'g' and 'ia' in l_w):
            where_to_put = l_w.find('a') - 1
    where_to_put += 1
    vs.insert(where_to_put, s)
    new_word = ''
    skip_next = False
    for pos, c in enumerate(vs[:-1]):
        if skip_next:
            skip_next = False
            continue
        if ''.join([c, vs[pos + 1]]) in sign_with_vowel:
            skip_next = True
            new_word += sign_with_vowel.get(''.join([c, vs[pos + 1]]))
        else:
            new_word += c
    if vs[-1] not in sign:
        new_word += vs[-1]
    return new_word


def uniform_sign(text):
    try:
        new_txt = re.sub(
            r'à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ',
            lambda x: dicchar[x.group()], text)
    except:
        new_txt = text
    new_txt = new_txt.encode("utf-8", errors="ignore").decode().split()
    new_txt = ' '.join([normalize_sign(word) for word in new_txt])
    return new_txt


def text_normalize(text: str) -> str:
    """
    Chuẩn hóa text đầu vào trước khi xử lý. Việc này giống như bước "lọc thô" trong pipeline cũ.
    Việc chuẩn hóa sâu hơn là không cần thiết, bởi nó làm ảnh hưởng đến khả năng tokenize, tách câu, ner, ... của spacy.
    Ví dụ của việc chuẩn hóa sâu hơn này:
    - lowercase hay lọc các kí tự punctuation, nó khiến việc nhận dạng ner và tách câu tệ đi, trong khi đối với các mô hình scoring,
    ta hoàn toàn có thể dùng spacy để "lọc nếu cần", ví dụ như sử dụng token.norm_ hay kiểm tra punctuation bằng token.is_punct.
    """
    if not text or not isinstance(text, str):
        raise Exception(f'Đầu vào của hàm chuẩn hóa text không hợp lệ:\t{type(text)}:\t{text}')

    pipeline = [clean_html,
                space_normalize,
                uniform_sign,
                clean_non_utf8,
                clean_abbreviation]
    # thứ tự của các hàm tiền xử lý
    for pipe in pipeline:
        if CONF['normalize'][pipe.__name__]:
            text = pipe(text)
    return text


if __name__ == '__main__':
    # unit test
    txt = u"""
     Các hàm trong phần cài đặt dưới có tham khảo tại `"Phân loại văn bản tiếng Việt sử dụng machine learning"
     <https://nguyenvanhieu.vn/phan-loai-van-ban-tieng-viet/>' của tác giả Nguyễn Văn Hiếu, với mục tiêu làm "mịn" hơn các
     văn bản tiếng việt để cải thiện các mô hình xử lý ngôn ngữ.
    
    Blockchain loại bỏ sự \ncần thiết phải có những thành phần trung gian đó. Công nghệ này ban đầu được phát triển để giúp 
    Bitcoin hoạt động độc lập và không \tcần tới sự kiểm soát của một ngân hàng Trung ương nào.
    Với sự hợp tác này, VeriME mang đến cho Avvanz dịch vụ Xác minh Nhận dạng số dựa trên nền tảng Blockchain, 
    phát triển dựa trên ứng dụng các công nghệ sinh trắc học tự động nhận so khớp với bản chụp các giấy tờ chứng thực 
    mà không cần gặp mặt, giúp thực hiện toàn bộ quá trình quản lý nhân viên, từ việc như kiểm tra lý lịch nhân viên 
    để phát triển tổng thể một cách nhanh chóng và bảo mật.    
    """
    txt = text_normalize(txt)
    print(txt)
