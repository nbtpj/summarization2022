import numpy as np

from util.concurrent import executor
import spacy
from spacy.lang.en.syntax_iterators import SYNTAX_ITERATORS
from transformers import pipeline
from typing import Iterable, Union
from util.normalize import text_normalize
from spacy.tokens import Doc
from transformers import AutoModel, AutoTokenizer
import torch
from config import CONF

spacy.prefer_gpu()

ner_detect = pipeline('ner', model="lhkhiem28/COVID-19-Named-Entity-Recognition-for-Vietnamese",
                      tokenizer="lhkhiem28/COVID-19-Named-Entity-Recognition-for-Vietnamese",
                      aggregation_strategy='simple')

device = "cuda:0" if torch.cuda.is_available() and CONF['system']['auto_gpu'] else "cpu"

pretrained_model = 'vinai/phobert-base' if CONF['system']['lang'] == 'vi' else 'dmis-lab/biobert-large-cased-v1.1'

bertmodel = AutoModel.from_pretrained(pretrained_model).to(device)
tokenizer = AutoTokenizer.from_pretrained(pretrained_model)


def bert_embed(doc: Doc) -> Doc:
    """
    Tạo ra các vector bert cho từng câu và từng từ trong câu.
    Các kết quả này được lưu như các user_data
    :param doc: đơn văn bản
    :return: các câu sau khi đã gán vector embedding và các từ trng câu đã gán
    """
    batch_size = CONF['system']['max_bert_embedding_batch']
    bert_pooler_vector = []
    bert_state_vector = []
    sents = list(doc.sents)
    max_len = min(CONF['system']['max_bert_sen_len'], tokenizer.model_max_length)
    for i in range(0, len(sents), batch_size):
        batch = sents[i:i + batch_size]
        sentences = tokenizer([[str(tok) for tok in sen] for sen in batch],
                              padding='max_length',
                              max_length=max_len,
                              is_split_into_words=True,
                              truncation=True,
                              return_tensors="pt").to(device)
        embedding_output = bertmodel(**sentences)
        token_vectors = embedding_output[0].cpu().detach().numpy()
        sentence_vectors = embedding_output[1].cpu().detach().numpy()
        bert_pooler_vector.append(sentence_vectors)
        bert_state_vector.append(token_vectors)
    bert_pooler_vector = np.concatenate(bert_pooler_vector, axis=0)
    bert_state_vector = np.concatenate(bert_state_vector, axis=0)
    doc.user_data['bert_sentence_vector'] = bert_pooler_vector
    doc.user_data['bert_token_vector'] = bert_state_vector
    return doc


def set_ner(doc: Doc) -> Doc:
    """
    Sử dụng một mô hình đã được train để tìm ner.
    :param doc: Doc cần gán ner
    :return: Doc sau khi đã gán ner
    """
    ner_results = ner_detect(doc.text_with_ws)
    ents = []
    p = 0
    h = str(doc)
    for ner in ner_results:
        try:
            start = h.index(ner['word'], p)
            end = start + len(ner['word'])
            p = end
            ents.append(doc.char_span(start, end, label=ner['entity_group']))
        except:
            pass

    try:
        doc.set_ents(ents)
    except:
        pass
    return doc


@spacy.Language.component("bert_embedding")
def embed_sentences(doc):
    """
    <<Tạo 1 pipeline trong lúc tạo một đối tượng spacy.token.Doc>>
    Sử dụng các mô hình bert để "nhúng" các vector embedding vào đối tượng doc.
    các vector này được "nhúng" trong các user_data của các đối tượng Span đặc trưng cho từng câu và từng token trong câu.
    Cách lấy các kết quả này: ví dụ lấy embedding của tất cả các câu:
    >> doc.user_data['bert_sentence_vector'] # lấy ra vector trung bình
            có dạng [số câu x độ dài vector embedding]
    >> doc.user_data['bert_token_vector'] # lấy ra embedding của tất cả các từ trong các câu (có padding)
            có dạng [số câu x lượng token (tất cả các câu đều được padding để có
            độ dài = min(độ dài lớn nhất mô hình cho phép, đô dài cấu hình sẵn)) x độ dài vector embedding]
    :param doc: doc cần embed
    :return: doc sau embed
    """
    bert_embed(doc)
    return doc


@spacy.Language.component("ner_")
def make_ner(doc):
    """
    <<Tạo 1 pipeline trong lúc tạo một đối tượng spacy.token.Doc>>
    Sử dụng hàm đã định nghĩa phía trên để gán ner cho đối tượng doc.
    Các đối tượng ner này có thể lấy ra bằng câu lệnh:
    >> doc.ents
    :param doc: Doc cần gán ner
    :return: Doc sau khi đã gán ner
    """
    doc = set_ner(doc)
    return doc


if CONF['system']['lang'] == 'en':
    nlp = spacy.load('en_core_web_sm')
else:
    nlp = spacy.load('vi_core_news_lg')
    nlp.vocab.get_noun_chunks = SYNTAX_ITERATORS['noun_chunks']
    """
    phần lấy các cụm dang từ của tiếng việt này hiện tại chưa được cài đặt, 
    hiện tại đang sử dụng tiếng anh, do đó không chính xác.
    Hàm này cần định nghĩa lại để có thể tính toán textrank
    """
    nlp.add_pipe("ner_", name='ner', last=True)

nlp.add_pipe("bert_embedding", name='bert_embedding', last=True)



def create_doc(text: Union[str, Iterable[str]]) -> Union[Doc, Iterable[Doc]]:
    """
    Tạo các đối tượng spacy doc (tiền xử lý)
    :param text: (các) text đầu vào
    :return: (các) đối tượng doc đã qua xử lý
    """
    if isinstance(text, str):
        text = [text]
    text = [text_normalize(t) for t in text]
    if len(text) == 1:
        return nlp(text[0])
    else:
        docs = [executor.submit(nlp, txt) for txt in text]
        for doc in docs:
            yield doc.result()


if __name__ == '__main__':
    text = [
        """
        Cơ quan CSĐT Bộ Công an vừa thi hành quyết định khởi tố, lệnh bắt tạm giam 3 bị can để điều tra về hành vi vi phạm quy định về đấu thầu gây hậu quả nghiêm trọng.

    Ba người gồm các ông Lâm Văn Tuấn, Giám đốc CDC tỉnh Bắc Giang; Phan Huy Văn, Giám đốc công ty Phan Anh có trụ sở tại TP Bắc Giang và Phan Thị Khánh Vân (chị ruột ông Văn).

    Cơ quan CSĐT Bộ Công an xác định, ông Lâm Văn Tuấn có hành vi thông đồng cấu kết với ông Phan Huy Văn, Phan Quốc Việt (TGĐ Công ty Việt Á) và các đối tượng liên quan vi phạm các quy định của luật đấu thầu khi tổ chức đấu thầu mua kit test Covid-19 do Công ty Việt Á sản xuất tổng giá trị hơn 148 tỷ đồng.

    Ông Văn và bà Vân còn thỏa thuận nhận trên 44 tỷ đồng tiền phần trăm ngoài hợp đồng do Công ty Việt Á chuyển. Vân đã chi một phần tiền cho ông Lâm Văn Tuấn, Giám đốc CDC Bắc Giang.""",
        """Theo các hồ sơ công khai và hai nguồn tin nắm rõ những thương vụ trên, một đơn vị của Ant Group đã mua 2 mảnh đất giảm giá ở Hàng Châu vào năm 2019, sau khi thâu tóm cổ phần tại 2 doanh nghiệp thanh toán di động thuộc sở hữu của em trai vị bí thư được đề cập đến trong bộ phim tài liệu.

    Dù bộ phim không nêu tên công ty của Jack Ma nhưng theo các hồ sơ công khai, đơn vị của Ant Group là doanh nghiệp đầu tư bên ngoài duy nhất vào một trong hai công ty trên và nằm trong số 3 doanh nghiệp đầu tư vào công ty còn lại.

    "Bản chất của quá trình chuyển giao quyền lợi như vậy là sự đổi chác giữa quyền và tiền", trích bình luận của phim tài liệu do Ủy ban Kiểm tra kỷ luật Trung ương của Đảng Cộng sản Trung Quốc sản xuất.

    Bộ phim đang gia tăng áp lực đối với Ant Group, trong bối cảnh tập đoàn công nghệ với hơn 1 tỷ người dùng đang vật lộn điều chỉnh hoạt động kinh doanh để phù hợp với các yêu cầu của nhà chức trách. Các cơ quan quản lý Trung Quốc đã đình chỉ kế hoạch phát hành cổ phiếu công khai lần đầu (IPO) trị giá 37 tỷ USD của tập đoàn vào năm 2020 và buộc họ phải tái cơ cấu."""
    ]
    docs = list(create_doc(text))
    for doc in docs:
        for token in doc:
            print('text:', token.text,
                  'norm_', token.norm_,
                  'lemma_:', token.lemma_,
                  'pos_:', token.pos_,
                  'tag_:', token.tag_,
                  'dep_:', token.dep_,
                  'shape_:', token.shape_,
                  'is_alpha:', token.is_alpha,
                  'is_stop:', token.is_stop, sep='\t')
