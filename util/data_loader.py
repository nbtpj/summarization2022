import os
from os.path import join
from typing import Iterable, Tuple, List, Dict
import json

from config import CONF
from util.spacy_config import create_doc, nlp, Doc
from spacy.tokens import DocBin

LOADER_CONF = CONF['data_loader']
"""
Tại đây, các liên quan đến khởi tạo các dữ liệu (tiền xử lý) sẽ được gọi đến.
"""
DATA_DIR = f"{os.path.dirname(__file__)}/../data"

VOCAB = set([])
DOCCORPUS = []  # tập doc chứa toàn bộ các văn bản trong data


def update_vocab(docs: Iterable[Doc]) -> Iterable[Doc]:
    update_doccorpus(docs)
    for doc in docs:
        new_tokens = [tok.norm_ for tok in doc]
        VOCAB.update(new_tokens)
    return docs


def update_doccorpus(docs: Iterable[Doc]) -> Iterable[Doc]:
    DOCCORPUS.extend(docs)
    return docs


def get_examples(conf: Dict = LOADER_CONF) -> Iterable[Tuple[Iterable[Doc], Iterable[Doc]]]:
    """
    load dữ liệu dựa trên các tham số truyền vàof.
    Mặc định các tham sốsosoayf được load từ file config.cfg
    Đây là hàm được sử dụng để lấy dữ liệu.
    :return: 1 generator trả về 2 giá trị X và Y, với mỗi X là Một cụm đa văn bản, và mỗi Y là Tập các tóm tắt của cụm đa văn bản tương ứng
    """
    if conf['is_new_data']:
        return build_data_from_json(**conf)
    else:
        return load(**conf)


def load(data_name, **kwargs) -> Iterable[Tuple[List[Doc], List[Doc]]]:
    """
    load dữ liệu đã tiền xử lý.
    :return: 1 generator trả về 2 giá trị X và Y tương tự như hàm get_examples()
    """

    data_path = join(DATA_DIR, data_name)
    js_data = json.load(open(join(data_path, 'origin.json'), encoding='utf-8'))
    if not os.path.exists(data_path):
        raise Exception(f"Dữ liệu {data_name} chưa được khởi tạo")
    for cluster_id in range(len(js_data)):
        docs_path = join(data_path, js_data[cluster_id]['path2Docs'])
        summ_path = join(data_path, js_data[cluster_id]['path2Summs'])
        if not os.path.exists(docs_path) or not os.path.exists(summ_path):
            continue
        doc_cluster = DocBin().from_disk(docs_path)
        summ_cluster = DocBin().from_disk(summ_path)

        yield update_vocab(list(doc_cluster.get_docs(nlp.vocab))), list(summ_cluster.get_docs(nlp.vocab))


def build_data_from_json(js_path,
                         data_name,
                         doc_txt_field,
                         summs,
                         summ_txt_field, **kwargs) -> Iterable[Tuple[List[Doc], List[Doc]]]:
    """
    tạo và tổ chức các bộ dữ liệu từ file json gốc.
    :param summ_txt_field: tên trường sử dụng để khởi tạo các đơn văn bản trong Y
    :param summs: tên trường sử dụng để khởi tạo Ys
    :param doc_txt_field:  tên trường sử dụng để khởi tạo các đơn văn bản trong X
    :param data_name: tên bộ dữ liệu
    :param js_path: đường dẫn (trong thư mục data) tới JSON data có cấu trúc như sau
     [
       {
           "docs":[
                    {
                       doc_txt_field: dữ liệu text sử dụng làm đầu vào cho tóm tắt,
                       ... các thành phần khác
                     }, ...
                ],
           summs: [ (nếu summs là null -> không bỏ qua giá trị này)
                   {
                        summ_txt_field: dữ liệu text sử dụng để đánh giá
                        ... các thành phần khác
                    }, ...
                    ],
                    ... các thành phần khác
        }
     ]
    :return: 1 generator trả về 2 giá trị X và Y tương tự như hàm get_examples()
    """

    js_data = json.load(open(join(DATA_DIR, js_path), encoding='utf-8'))
    data_path = join(DATA_DIR, data_name)
    if not os.path.exists(data_path):
        os.mkdir(data_path)
    for cluster_id in range(len(js_data)):
        cluster = js_data[cluster_id]
        docs_path = join(data_path, f'docs-{cluster_id}.spacy')
        if not os.path.exists(docs_path):
            docs_txt = [doc[doc_txt_field] for doc in cluster['docs']]
            preprocessed_docs = list(create_doc(docs_txt))
            doc_cluster = DocBin(docs=preprocessed_docs, store_user_data=True)
            doc_cluster.to_disk(docs_path)
        else:
            doc_cluster = DocBin().from_disk(docs_path)
            preprocessed_docs = update_vocab(list(doc_cluster.get_docs(nlp.vocab)))
        js_data[cluster_id]['path2Docs'] = f'docs-{cluster_id}.spacy'

        summ_path = join(data_path, f'summs-{cluster_id}.spacy')
        if not os.path.exists(summ_path):
            summs_txt = [ref[summ_txt_field] for ref in cluster[summs]]
            preprocessed_summs = list(create_doc(summs_txt))
            summ_cluster = DocBin(docs=preprocessed_summs, store_user_data=True)
            summ_cluster.to_disk(summ_path)
        else:
            summ_cluster = DocBin().from_disk(summ_path)
            preprocessed_summs = list(summ_cluster.get_docs(nlp.vocab))
        js_data[cluster_id]['path2Summs'] = f'summs-{cluster_id}.spacy'

        if cluster_id == len(js_data) - 1:
            json.dump(js_data, open(join(data_path, 'origin.json'), 'w+', encoding='utf8'), indent=2,
                      ensure_ascii=False)
        yield update_vocab(preprocessed_docs), preprocessed_summs
